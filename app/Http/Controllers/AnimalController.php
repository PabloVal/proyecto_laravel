<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AnimalController extends Controller
{
   	 public function index(){

		return view("animales.index");
	}

	public function show(Animal $animal){

		return view("animales.show, ['animal' => $animal]");
	}

	public function edit(Animal $animal){

		return view("animales.edit, ['animal' => $animal]");
	}


	public function create(){

		return view("animales.create");
	}

}
