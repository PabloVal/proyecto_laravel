<?php

use Illuminate\Support\Facades\Route;
use App\Html\Controllers\InicioController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'InicioController@inicio');

Route::get('animales/index', 'AnimalController@index');

Route::get('animales/{animal}', 'AnimalController@show');

Route::get('animales/crear', 'AnimalController@create');

Route::get('animales/{animal}/editar', 'AnimalController@edit' );
